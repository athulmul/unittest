const chai = require("chai");
const sinon = require("sinon");
const expect = chai.expect;
const faker = require("faker");
const { UserModel } = require("../database");
const UserRepository = require("./user.repository");
const { fake } = require("faker");
const { stub } = require("sinon");

describe("UserRepository", function() {
    const stubValue = {
        id: faker.random.id(),
        name: faker.name.findName(),
        email: faker.internet.email(),
        createdAt: faker.date.past(),
        updatedAt: faker.date.past()

    };


    describe("Create", () =>{
        it(" should add new user to db", async ()=>{
            const stub = sinon.stub(UserModel, "create").returns(stubValue)
            const userRepository = new userRepository()
            const user = await userRepository.getUser(stubValue.id)
            expect(stub.calledOnce).to.be.true
            expect(user.id).to.equal(stubValue.id);
            expect(user.name).to.equal(stubValue.name);
            expect(user.email).to.equal(stubValue.email);
            expect(user.createdAt).to.equal(stubValue.createdAt);
            expect(user.updatedAt).to.equal(stubValue.updatedAt);
        })
    })
})