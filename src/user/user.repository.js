const UserModel = require("../database")

class UserRepository {
    constructor(){
        this.user = UserModel
        this.user.sync({force: true})
    }
    
    async create(email, name){
        return this.user.create({email, name})
    }

    async getUser(id){
        return this.user.findOne({id})
    }
}

module.exports = UserRepository